import XMonad

import XMonad.Util.SpawnOnce
import XMonad.Layout.Gaps
import XMonad.Layout.Maximize
import XMonad.Hooks.EwmhDesktops

import System.IO
import System.Exit

import qualified Data.Map as M
import qualified XMonad.StackSet as W hiding (swapDown, swapUp)

--Default Applications
myTerminal = "alacritty"
myBrowser = "qutebrowser --backend webengine"
myFileManager = "alacritty -e ranger"

--Custom Functions
swapUp'  (W.Stack t (l:ls) rs) = W.Stack t ls (l:rs)
swapUp'  (W.Stack t []     rs) = W.Stack t (rot $ reverse rs) []
    where rot (x:xs) = xs ++ [x]
          rot _ = []

swapUp = W.modify' swapUp'

reverseStack (W.Stack t ls rs) = W.Stack t rs ls

swapDown = W.modify' (reverseStack . swapUp' . reverseStack)

--Main
main = do
    xmonad $ ewmh 
           $ defaults

--Setting all the things
defaults = def
    { terminal = myTerminal
    , modMask  = mod1Mask
    , borderWidth = 1
    , startupHook = myStartupHook
    , keys = myKeys
    , layoutHook = myLayoutHook
    }

--LayoutHook
myLayoutHook = gaps [(U,32)] $ Tall 1 (3/100) (1/2) ||| Full 

--Autostart
myStartupHook = do
    spawn "$HOME/.xmonad/autostart.sh"

--Keybindings
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm, xK_b), spawn myBrowser)
    , ((modm, xK_m), withFocused $ sendMessage . maximizeRestore)
    , ((modm, xK_j), windows Main.swapDown )
    , ((modm, xK_k), windows Main.swapUp )
    , ((modm, xK_t), withFocused $ windows . W.sink)
    , ((modm, xK_p), spawn "rofi -show drun -theme solarized")
    , ((modm, xK_space), sendMessage NextLayout)
    , ((modm, xK_f), spawn myFileManager)

    , ((modm .|. shiftMask, xK_m), spawn "alacritty -e aerc")
    , ((modm .|. shiftMask, xK_p), spawn "/home/enderger/.xmonad/autostart.sh")
    , ((modm .|. shiftMask, xK_Return), spawn myTerminal)
    , ((modm .|. shiftMask, xK_c), kill)
    , ((modm .|. shiftMask, xK_Escape), io (exitWith ExitSuccess))
    , ((modm .|. shiftMask, xK_s), spawn "systemctl suspend")
    ]
    ++
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1..xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
