#!/bin/sh
picom &
nitrogen --restore &
lxqt-policykit-agent &
xscreensaver &
polybar bar1 &
polybar bar2 &
